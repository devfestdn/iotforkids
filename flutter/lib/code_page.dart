import 'dart:io';
import 'dart:typed_data';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screen/screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'blue_mgr.dart';
import 'discovery_page.dart';
import 'screen_util.dart';

class CodePage extends StatefulWidget {
  CodePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _CodePageState createState() => _CodePageState();
}

class _CodePageState extends State<CodePage> {
  String _binPath = "/sdcard/Download";
  String url = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Screen.keepOn(true);
    WebViewController _controller;
    bool _onBlocksTab = true;

    return Scaffold(
      body: Center(
        child: new Stack(
          children: <Widget>[
            new WebView(
              initialUrl: 'file:///android_asset/html/index.html',
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
              },
              javascriptChannels: Set.from([
                JavascriptChannel(
                    name: 'Arduino',
                    onMessageReceived: (JavascriptMessage message) async {
                      print(message.message);
                      url = message.message;
                    })
              ]),
              onPageFinished: (String url) {
                print('Page finished loading: $url');
              },
              initialMediaPlaybackPolicy: AutoMediaPlaybackPolicy.always_allow,
            ),
            new Positioned(
              top: 20 * ScreenUtil().scale,
              right: 140 * ScreenUtil().scale,
              child: Transform.scale(
                scale: ScreenUtil().scale * 0.85,
                child: FloatingActionButton(
                  heroTag: "btCompile",
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                  onPressed: () async {
                    await _controller.evaluateJavascript("Code.uploadClick();");
                  },
                ),
              ),
            ),
            new Positioned(
              top: 20 * ScreenUtil().scale,
              right: 80 * ScreenUtil().scale,
              child: Transform.scale(
                scale: ScreenUtil().scale * 0.85,
                child: FloatingActionButton(
                  heroTag: "btView",
                  child: Icon(
                    Icons.remove_red_eye,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    if (_onBlocksTab) {
                      _controller
                          .evaluateJavascript("Code.tabClick(\"arduino\");");
                      _onBlocksTab = false;
                    } else {
                      _controller
                          .evaluateJavascript("Code.tabClick(\"blocks\");");
                      _onBlocksTab = true;
                    }
                  },
                ),
              ),
            ),
            new Positioned(
              top: 20 * ScreenUtil().scale,
              right: 20 * ScreenUtil().scale,
              child: Transform.scale(
                scale: ScreenUtil().scale * 0.85,
                child: FloatingActionButton(
                  heroTag: "btUpload",
                  child: Icon(
                    Icons.file_upload,
                    color: Colors.white,
                  ),
                  onPressed: () async {
                    await _connectBluetooth().then((data) =>
                        _sendUrl().then((data) => BlueMgr().disconnect()));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<String> get _cachePath async {
    final directory = await getTemporaryDirectory();
    return directory.path;
  }

  Future<void> _connectBluetooth() async {
    if (!BlueMgr().isConnected) {
      final BluetoothDevice selectedDevice = await Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) {
        return DiscoveryPage();
      }));
      await BlueMgr().connect(selectedDevice.address);
    }
  }

  void _onDataReceived(Uint8List data) async {
    int backspacesCounter = 0;
    data.forEach((byte) {
      if (byte == 8 || byte == 127) {
        backspacesCounter++;
      }
    });
    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    String dataString = String.fromCharCodes(buffer);
    print("Received data = $dataString");
    _sendBinFile();
  }

  Future<void> _sendBinFile() async {
    Uint8List binContent = await _readBinFile();
    await BlueMgr().sendBytes(binContent);
  }

  Future<void> _sendUrl() async {
    await BlueMgr().sendString(url);
  }

  Future<Uint8List> _readBinFile() async {
    final file = File("$_binPath/i4k.bin");
    var contents = await file.readAsBytes();
    await Fluttertoast.showToast(msg: "read done. length = ${contents.length}");
    return contents;
  }
}
