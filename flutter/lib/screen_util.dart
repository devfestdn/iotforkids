import 'dart:math';
import 'package:flutter/widgets.dart';

class ScreenUtil {
  static final ScreenUtil _singleton = new ScreenUtil._internal();

  factory ScreenUtil() {
    return _singleton;
  }

  ScreenUtil._internal();

  double height = 360.0;
  double width = 640.0;
  double scaleX = 1.0;
  double scaleY = 1.0;
  double scale = 1.0;

  static MediaQueryData _mediaQueryData;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);

    width = _mediaQueryData.size.width;
    height = _mediaQueryData.size.height;

    scaleX = width / 640;
    scaleY = height / 360;
    scale = min(scaleX, scaleY);
  }
}
