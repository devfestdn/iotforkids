import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class BlueMgr {
  static final BlueMgr _singleton = new BlueMgr._internal();

  factory BlueMgr() {
    return _singleton;
  }

  BlueMgr._internal();

  BluetoothConnection connection;
  bool get isConnected => connection != null && connection.isConnected;
  bool _isContinueSending = true;
  ValueNotifier<bool> _valueNotifier = ValueNotifier(true);

  Future<void> connect(String address) async {
    if (isConnected) {
      await connection.close();
    }
    if (address == null) {
      Fluttertoast.showToast(msg: "Connect -> no device selected");
    } else {
      print('Connect -> selected ' + address);
      await BluetoothConnection.toAddress(address).then((_connection) {
        Fluttertoast.showToast(msg: "Connected to the device");
        connection = _connection;
      }).catchError((error) {
        Fluttertoast.showToast(msg: "Cannot connect, exception occured");
        print(error);
      });
    }
  }

  void setListen(Function func) {
    connection.input.listen(func);
  }

  Future<void> sendString(String text) async {
    text = text.trim();
    print("url = $text");
    if (text.length > 0) {
      try {
        Uint8List data = utf8.encode(text);
        connection.output.add(data);
        connection.output.allSent;
      } catch (e) {
        print(e);
      }
    }
  }

  Future<void> sendBytes(Uint8List data) async {
    if (isConnected) {
      try {
        // set listener receive data from bluetooth
        // connection.input.listen((Uint8List data) async {
        //   // _isContinueSending = true;
        //   print("receive data = $data");

        //   _valueNotifier.value = true;
        //   _valueNotifier.addListener(() {
        //     _isContinueSending = _valueNotifier.value;
        //   });
        //   _valueNotifier.notifyListeners();
        // });

        print("size = ${data.length}");
        // start count sending time
        final startTime = DateTime.now().millisecondsSinceEpoch;
        final step = 100;
        int count = step;
        int checkTimeout = 0;

        while (true) {
          connection.output.add(data.sublist(count - step, count));
          sleep(const Duration(milliseconds: 10));
          // print("[$count] send part ...");

          // waiting for board write file until receive any bluetooth data from board
          if (count % 100000 == 0) {
            print("[$count] send part ...");
            await connection.output.allSent;
            sleep(const Duration(milliseconds: 500));
            // if (count == 200000) {
            //   sleep(const Duration(seconds: 1));
            // }

            // _valueNotifier.value = false;

            // while (_isContinueSending) {
            //   _valueNotifier.addListener(() {
            //     print("_isContinueSending = $_isContinueSending");
            //     _isContinueSending = _valueNotifier.value;
            //     print("_isContinueSending = $_isContinueSending");
            //   });
            //   sleep(const Duration(seconds: 1));
            //   checkTimeout++;
            //   if (checkTimeout >= 10) {
            //     print("Timeout!");
            //     break;
            //   }
            // }
            // if (checkTimeout >= 10) {
            //   break;
            // }
          }
          count += step;
          // send last data
          if (count >= data.length) {
            print("send $count last part");
            sleep(const Duration(seconds: 1));

            connection.output.add(data.sublist(count - step, data.length));
            sleep(const Duration(milliseconds: 10));
            await connection.output.allSent;
            break;
          }
        }

        // Count sending time
        final sendingTime = DateTime.now().millisecondsSinceEpoch - startTime;
        final timeStr =
            "${(sendingTime / 60000).round()}'${(sendingTime % 60000 / 1000).round()}\"${sendingTime % 60000 % 1000}";
        print("Data size = ${data.length}. Sending time = $timeStr");

        // Show result
        if (checkTimeout >= 10) {
          print("Send failed!");
          await Fluttertoast.showToast(msg: "Timeout!");
        } else {
          print("Done. Send success");
          await Fluttertoast.showToast(
              msg: "Send success! Sending time = $timeStr");
        }
      } catch (e) {
        Fluttertoast.showToast(msg: "Send failed.\n$e");
        print(e);
      }
    } else {
      Fluttertoast.showToast(msg: "no connect");
    }
  }

  Future<void> requestESP32UpdateFirmware() async {
    Uint8List data = Uint8List.fromList([
      192,
      0,
      8,
      36,
      0,
      0,
      0,
      0,
      0,
      7,
      7,
      18,
      32,
      84,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      84,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      84,
      85,
      85,
      85,
      85,
      85,
      85,
      85,
      192
    ]);
    await sendBytes(data);
  }

  Future<void> disconnect() async {
    await connection.finish();
  }
}
