import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screen/screen.dart';
import 'code_page.dart';
import 'screen_util.dart';
import 'package:url_launcher/url_launcher.dart';

class MainMenuPage extends StatefulWidget {
  MainMenuPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MainMenuPageState createState() => _MainMenuPageState();
}

class _MainMenuPageState extends State<MainMenuPage> {
  @override
  void initState() {
    super.initState();
    PermissionHandler().requestPermissions([PermissionGroup.storage]);
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  @override
  dispose() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Screen.keepOn(true);
    ScreenUtil().init(context);
    debugPaintSizeEnabled = false;
    debugPaintLayerBordersEnabled = false;
    debugPaintBaselinesEnabled = false;
    debugPaintPointersEnabled = false;

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg.webp"),
          fit: BoxFit.cover,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                verticalDirection: VerticalDirection.up,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    width: 100 * ScreenUtil().scale,
                    height: 45 * ScreenUtil().scale,
                    child: FloatingActionButton.extended(
                      heroTag: "btCode",
                      icon: Icon(
                        Icons.code,
                        size: ScreenUtil().scale * 16,
                      ),
                      label: Text(
                        "Code",
                        textScaleFactor: ScreenUtil().scale,
                      ),
                      onPressed: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return CodePage();
                        }));
                      },
                    ),
                  ),
                  SizedBox(width: 10 * ScreenUtil().scale),
                  SizedBox(
                    width: 100 * ScreenUtil().scale,
                    height: 45 * ScreenUtil().scale,
                    child: FloatingActionButton.extended(
                      heroTag: "btHelp",
                      icon: Icon(
                        Icons.help,
                        size: ScreenUtil().scale * 16,
                      ),
                      label: Text(
                        "Help",
                        textScaleFactor: ScreenUtil().scale,
                      ),
                      onPressed: () {
                        _launchURL();
                      },
                    ),
                  ),
                  SizedBox(width: 10 * ScreenUtil().scale),
                  SizedBox(
                    width: 100 * ScreenUtil().scale,
                    height: 45 * ScreenUtil().scale,
                    child: FloatingActionButton.extended(
                      heroTag: "btAbout",
                      icon: Icon(
                        Icons.info,
                        size: ScreenUtil().scale * 16,
                      ),
                      label: Text(
                        "About",
                        textScaleFactor: ScreenUtil().scale,
                      ),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  _launchURL() async {
    const url = 'https://github.com/google/blockly/wiki';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
