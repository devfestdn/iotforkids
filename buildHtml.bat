@echo off
REM copy files from libs to html folder
call copy .\blockly\blocks_compressed.js .\html\js\
call copy .\blockly\appengine\storage.js .\html\js\
call copy .\blockly\blockly_compressed.js .\html\js\
call copy .\blockly\msg\js\en.js .\html\msg\
call xcopy /e/y .\blockly\media\* .\html\media\

REM copy files from html to assets folder
call xcopy /e/y .\html\* .\flutter\android\app\src\main\assets\html\