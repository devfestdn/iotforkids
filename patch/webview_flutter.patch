diff --git a/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/FlutterWebView.java b/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/FlutterWebView.java
index a7f2db30..ba86f41f 100644
--- a/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/FlutterWebView.java
+++ b/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/FlutterWebView.java
@@ -5,12 +5,15 @@
 package io.flutter.plugins.webviewflutter;
 
 import android.annotation.TargetApi;
+import android.app.Activity;
 import android.content.Context;
 import android.hardware.display.DisplayManager;
 import android.os.Build;
 import android.os.Handler;
 import android.view.View;
+import android.webkit.WebChromeClient;
 import android.webkit.WebStorage;
+import android.webkit.WebView;
 import android.webkit.WebViewClient;
 import io.flutter.plugin.common.BinaryMessenger;
 import io.flutter.plugin.common.MethodCall;
@@ -24,7 +27,7 @@ import java.util.Map;
 
 public class FlutterWebView implements PlatformView, MethodCallHandler {
   private static final String JS_CHANNEL_NAMES_FIELD = "javascriptChannelNames";
-  private final InputAwareWebView webView;
+  private final WebView webView;
   private final MethodChannel methodChannel;
   private final FlutterWebViewClient flutterWebViewClient;
   private final Handler platformThreadHandler;
@@ -32,6 +35,7 @@ public class FlutterWebView implements PlatformView, MethodCallHandler {
   @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
   @SuppressWarnings("unchecked")
   FlutterWebView(
+      final Activity activity,
       final Context context,
       BinaryMessenger messenger,
       int id,
@@ -42,7 +46,7 @@ public class FlutterWebView implements PlatformView, MethodCallHandler {
     DisplayManager displayManager =
         (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
     displayListenerProxy.onPreWebViewInitialization(displayManager);
-    webView = new InputAwareWebView(context, containerView);
+    webView = new WebView(activity);
     displayListenerProxy.onPostWebViewInitialization(displayManager);
 
     platformThreadHandler = new Handler(context.getMainLooper());
@@ -75,26 +79,6 @@ public class FlutterWebView implements PlatformView, MethodCallHandler {
     return webView;
   }
 
-  // @Override
-  // This is overriding a method that hasn't rolled into stable Flutter yet. Including the
-  // annotation would cause compile time failures in versions of Flutter too old to include the new
-  // method. However leaving it raw like this means that the method will be ignored in old versions
-  // of Flutter but used as an override anyway wherever it's actually defined.
-  // TODO(mklim): Add the @Override annotation once flutter/engine#9727 rolls to stable.
-  public void onInputConnectionUnlocked() {
-    webView.unlockInputConnection();
-  }
-
-  // @Override
-  // This is overriding a method that hasn't rolled into stable Flutter yet. Including the
-  // annotation would cause compile time failures in versions of Flutter too old to include the new
-  // method. However leaving it raw like this means that the method will be ignored in old versions
-  // of Flutter but used as an override anyway wherever it's actually defined.
-  // TODO(mklim): Add the @Override annotation once flutter/engine#9727 rolls to stable.
-  public void onInputConnectionLocked() {
-    webView.lockInputConnection();
-  }
-
   @Override
   public void onMethodCall(MethodCall methodCall, Result result) {
     switch (methodCall.method) {
@@ -246,6 +230,8 @@ public class FlutterWebView implements PlatformView, MethodCallHandler {
               flutterWebViewClient.createWebViewClient(hasNavigationDelegate);
 
           webView.setWebViewClient(webViewClient);
+          webView.setWebChromeClient(new WebChromeClient());
+          webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
           break;
         case "debuggingEnabled":
           final boolean debuggingEnabled = (boolean) settings.get(key);
@@ -295,7 +281,6 @@ public class FlutterWebView implements PlatformView, MethodCallHandler {
   @Override
   public void dispose() {
     methodChannel.setMethodCallHandler(null);
-    webView.dispose();
     webView.destroy();
   }
 }
diff --git a/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFactory.java b/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFactory.java
index 6fdc36fb..9fa18d74 100644
--- a/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFactory.java
+++ b/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFactory.java
@@ -4,6 +4,7 @@
 
 package io.flutter.plugins.webviewflutter;
 
+import android.app.Activity;
 import android.content.Context;
 import android.view.View;
 import io.flutter.plugin.common.BinaryMessenger;
@@ -13,11 +14,13 @@ import io.flutter.plugin.platform.PlatformViewFactory;
 import java.util.Map;
 
 public final class WebViewFactory extends PlatformViewFactory {
+  private final Activity activity;
   private final BinaryMessenger messenger;
   private final View containerView;
 
-  WebViewFactory(BinaryMessenger messenger, View containerView) {
+  WebViewFactory(Activity activity, BinaryMessenger messenger, View containerView) {
     super(StandardMessageCodec.INSTANCE);
+    this.activity = activity;
     this.messenger = messenger;
     this.containerView = containerView;
   }
@@ -26,6 +29,6 @@ public final class WebViewFactory extends PlatformViewFactory {
   @Override
   public PlatformView create(Context context, int id, Object args) {
     Map<String, Object> params = (Map<String, Object>) args;
-    return new FlutterWebView(context, messenger, id, params, containerView);
+    return new FlutterWebView(activity, context, messenger, id, params, containerView);
   }
 }
diff --git a/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFlutterPlugin.java b/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFlutterPlugin.java
index 17177541..3e33b732 100644
--- a/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFlutterPlugin.java
+++ b/packages/webview_flutter/android/src/main/java/io/flutter/plugins/webviewflutter/WebViewFlutterPlugin.java
@@ -14,7 +14,7 @@ public class WebViewFlutterPlugin {
         .platformViewRegistry()
         .registerViewFactory(
             "plugins.flutter.io/webview",
-            new WebViewFactory(registrar.messenger(), registrar.view()));
+            new WebViewFactory(registrar.activity(), registrar.messenger(), registrar.view()));
     FlutterCookieManager.registerWith(registrar.messenger());
   }
 }
diff --git a/packages/webview_flutter/lib/src/webview_android.dart b/packages/webview_flutter/lib/src/webview_android.dart
index f7afcc06..479eee16 100644
--- a/packages/webview_flutter/lib/src/webview_android.dart
+++ b/packages/webview_flutter/lib/src/webview_android.dart
@@ -35,7 +35,6 @@ class AndroidWebView implements WebViewPlatform {
       // https://github.com/flutter/flutter/issues/24584 - the text selection
       // handles are not showing.
       // TODO(amirh): remove this when the issues above are fixed.
-      onLongPress: () {},
       excludeFromSemantics: true,
       child: AndroidView(
         viewType: 'plugins.flutter.io/webview',
