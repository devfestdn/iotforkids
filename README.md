# IOT FOR KIDS - DEVFEST 2019

## Getting Started

---
### Init submodule


```
git submodule update --init --recursive
```

---
### Patch plugin for webview_flutter to fix some issues


```
cd .\plugins\
git apply ..\patch\webview_flutter.patch
```

---
### Build html file for blockly

```
.\buildHtml.bat
```
---
### Build and run on device

```
cd flutter
flutter run
```