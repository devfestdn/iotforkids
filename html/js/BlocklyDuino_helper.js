/**
 * @fileoverview JavaScript for BlocklyDuino.
 * @author dieu.cit@gmail.com
 * 
 */
'use strict';

/**
 * Prepare code to send it to function Code.uploadCode.
 */
Code.uploadClick = function () {
  var code = Blockly.Arduino.workspaceToCode();
  Code.uploadCode(code, function (status, info) {
    if (status == 200) {
      alert("BUILD SUCCESS\n" + info.output);
      var link = info.bin.replace("aifablab.vn", "149.28.140.14:8005");
      link = link.replace("https", "http");
      if (typeof Arduino !== "undefined" && Arduino !== null) {
        Arduino.postMessage(link);
      } else {
        console.log(link);
      }
    } else {
      alert("BUILD ERROR\n" + info.output);
    }
  });
};

/**
 * Send code to Python server on 8080 port.
 */
Code.uploadCode = function (code, callback) {
  var target = document.getElementById('content_' + Code.selected);
  var spinner = new Spinner().spin(target);
  var url = "https://aifablab.vn/api/ino/";

  fetch(url, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      core: '',
      fqbn: '',
      libs: '',
      code: code,
    })
  }).then(response => response.json().then(data => ({ status: response.status, body: data })))
    .then(function (result) {
      spinner.stop();
      callback(result.status, result.body);
    })
};

/**
 * Send blank code to Python server to 'clean' Arduino card firmware.
 */
Code.resetClick = function () {
  var code = "void setup() {} void loop() {}";

  uploadCode(code, function (status, errorInfo) {
    if (status != 200) {
      alert("Error resetting program: " + errorInfo);
    }
  });
};

/**
* Save Arduino generated code to local file.
*/
Code.saveCode = function () {
  var fileName = window.prompt('What would you like to name your file?', 'BlocklyDuino')
  //doesn't save if the user quits the save prompt
  if (fileName) {
    var blob = new Blob([Blockly.Arduino.workspaceToCode()], { type: 'text/plain;charset=utf-8' });
    saveAs(blob, fileName + '.ino');
  }
};


/**
 * Save blocks to local file.
 * better include Blob and FileSaver for browser compatibility
 */
Code.saveXML = function () {
  var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
  var data = Blockly.Xml.domToText(xml);
  var fileName = window.prompt('What would you like to name your file?', 'BlocklyDuino');
  if (fileName) {
    var blob = new Blob([data], { type: 'text/xml' });
    saveAs(blob, fileName + ".xml");
  }
};

/**
 * Load blocks from local file.
 */
Code.loadXML = function (event) {
  var files = event.target.files;
  // Only allow uploading one file.
  if (files.length != 1) {
    return;
  }

  // FileReader
  var reader = new FileReader();
  reader.onloadend = function (event) {
    var target = event.target;
    // 2 == FileReader.DONE
    if (target.readyState == 2) {
      try {
        var xml = Blockly.Xml.textToDom(target.result);
      } catch (e) {
        alert('Error parsing XML:\n' + e);
        return;
      }
      var count = Blockly.mainWorkspace.getAllBlocks().length;
      if (count && confirm('Replace existing blocks?\n"Cancel" will merge.')) {
        Blockly.mainWorkspace.clear();
      }
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
    }
    // Reset value of input after loading because Chrome will not fire
    // a 'change' event if the same file is loaded again.
    document.getElementById('load').value = '';
  };
  reader.readAsText(files[0]);
};

/**
 * Restore code blocks from localStorage, needed for XML load.
 */
Code.restore_blocks = function () {
  if ('localStorage' in window && window.localStorage.arduino) {
    var xml = Blockly.Xml.textToDom(window.localStorage.arduino);
    Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
  }
};

/*
 * auto save and restore blocks
 * for XML load
 */
Code.auto_save_and_restore_blocks = function () {
  // Restore saved blocks in a separate thread so that subsequent
  // initialization is not affected from a failed load.
  window.setTimeout(Code.restore_blocks, 0);
  // Hook a save function onto unload.
  bindEvent(window, 'unload', backup_blocks);
  tabClick(selected);

  // Init load event.
  var loadInput = document.getElementById('loadXMLaction');
  loadInput.addEventListener('change', loadXML, false);
  document.getElementById('loadXMLfakeButton').onclick = function () {
    loadInput.click();
  };
};